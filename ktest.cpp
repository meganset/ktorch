#include "ktorch.h"

#ifdef __clang__
# pragma clang diagnostic push
# pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"    // FORWARD_HAS_DEFAULT_ARGS
#endif

#include "torch/script.h"
#include "knn.h"
namespace nn=torch::nn;
namespace fnn=torch::nn::functional;

KAPI testhalf(K x) {
 KTRY
  auto t=torch::tensor({1.0,2.0,3.0});
  std::cerr << t << "\n";
  t=t.cuda();
  std::cerr << t << "\n";
  t=t.to(torch::kFloat16);
  std::cerr << t << "\n";
  return (K)0;
 KCATCH("testhalf");
}

// -------------------------------------------------------------------
// ewa
// -------------------------------------------------------------------
static void ewacalc(double a,bool p,const Module& src,Module& tgt) {
 auto d=p ? tgt.named_parameters() : tgt.named_buffers();
 for(const auto& x:p ? src.named_parameters() : src.named_buffers()) {
  if(!x.value().is_floating_point()) continue;
  auto *y=d.find(x.key());
  TORCH_CHECK(y, "ema: unable to find target ",(p ? "parameter" : "buffer"),": `",x.key());
  TORCH_CHECK(x.value().sizes()==y->sizes(), "ema: size mismatch, source size ",x.value().sizes()," vs target ",y->sizes());
  y->mul_(a).add_(x.value().mul(1.0-a));
 }
}

KAPI ewa(K x) {
 KTRY
  torch::NoGradGuard g; double a;
  TORCH_CHECK(!x->t, "ewa: not implemented for ",kname(x));
  TORCH_CHECK(x->n==3, "ewa: expecting 3 args, (alpha;source;target), given ",x->n," arg(s)");
  TORCH_CHECK(xdouble(x,0,a), "ewa: 1st arg of alpha (the exponential moving average factor) is expected as double, given ",kname(x,0));
  auto *m=xmodel(x,1); auto *q=m ? m->kmodule() : xmodule(x,1);
  TORCH_CHECK(q, "ewa: 2nd arg of source module or model expected, given ",kname(x,1));
  const auto& src=q->module();
  m=xmodel(x,2); q=m ? m->kmodule() : xmodule(x,2);
  TORCH_CHECK(q, "ewa: 3rd arg of target module or model expected, given ",kname(x,2));
  auto& tgt=q->module();
  ewacalc(a, true,  src, tgt);
  ewacalc(a, false, src, tgt);
  return (K)0;
 KCATCH("ewa");
}

KAPI whiten(K x) {
 KTRY
  Tensor e,t,v;
  bool b=xten(x,t); if(!b) t=kput(x);
  int64_t h=3, w=3, c=t.size(1);
  auto p=t.unfold(2,h,1).unfold(3,w,1);
  p=p.transpose(1,3).reshape({-1,c,h,w}).to(torch::kFloat);
  const auto& s=p.sizes();
  auto n=s[0]; c=s[1],h=s[2],w=s[3];
  auto X=p.reshape({n,c*h*w});
  X=X/std::sqrt(X.size(0)-1);
  std::tie(e,v)=torch::linalg_eigh(X.t().mm(X));
  e=e.flip(0);
  v=v.t().reshape({c*h*w,c,h,w}).flip(0);
  return kresult(b, (v / torch::sqrt(e + 1e-2).view({-1,1,1,1})));
 KCATCH("whiten");
}

KAPI mixup(K a) {
 KTRY
  Tensor x,y; xtenarg(a,x,y);
  double a=1.0;
  int64_t n=10;
  auto l=torch::_sample_dirichlet(torch::tensor({a,a}))[0].item<double>();
  TensorVector v;
  x=x.is_floating_point() ? x.clone() : x.to(torch::kFloat);
  v.emplace_back(x.clone().mul_(l).add_(x.roll(1,0).mul(1-l)));
  y=y.dim()>1 ? y.clone() : torch::nn::functional::one_hot(y,n).to(x.is_floating_point() ? x.scalar_type() : torch::kFloat);
  v.emplace_back(y.mul_(l).add_(y.roll(1,0).mul(1-l)));
  return kvec(v);
 KCATCH("mixup");
}

static const std::vector<torch::indexing::TensorIndex> cutindex(int64_t h,int64_t w,double& l) {
 using namespace torch::indexing;
 auto r=0.5 * std::sqrt(1-l);
 auto rw = (int64_t)floor(r*w);
 auto rh = (int64_t)floor(r*h);
 auto rx=torch::randint(w,1).item<int64_t>();
 auto ry=torch::randint(h,1).item<int64_t>();
 auto x1=rx-rw; if(x1<0) x1=0; auto x2=rx+rw; if(x2>w) x2=w;
 auto y1=ry-rh; if(y1<0) y1=0; auto y2=ry+rh; if(y2>h) y2=h;
 l=(x2-x1) * (y2-y1);
 l=1.0 - l/(w * h);
 return {Ellipsis, Slice(y1,y2), Slice(x1,x2)};
}

KAPI cutmix(K a) {
 KTRY
  Tensor x,y; xtenarg(a,x,y);
  double a=1.0;
  int64_t n=10;
  auto l=torch::_sample_dirichlet(torch::tensor({a,a}))[0].item<double>();
  auto i=cutindex(x.size(-2),x.size(-1),l);
  TensorVector v;
  v.emplace_back(x.clone().index_put_(i, x.roll(1,0).index(i)));
  y=y.dim()>1 ? y.clone() : torch::nn::functional::one_hot(y,n).to(x.is_floating_point() ? x.scalar_type() : torch::kFloat);
  v.emplace_back(y.mul_(l).add_(y.roll(1,0).mul(1-l)));
  return kvec(v);
 KCATCH("cutmix");
}

KAPI cutmix1(K x) {
 KTRY
  Tensor t;
  if(!xten(x,t)) t=kput(x);
  auto h=t.size(-2);
  auto w=t.size(-1);
  double a=1.0;
  auto l=torch::_sample_dirichlet(torch::tensor({a,a}))[0].item<double>();
  auto r=0.5 * std::sqrt(1-l);
  auto rw = (int64_t)floor(r*w);
  auto rh = (int64_t)floor(r*h);
  auto rx=torch::randint(w,1).item<int64_t>();
  auto ry=torch::randint(h,1).item<int64_t>();
  auto x1=rx-rw; if(x1<0) x1=0;
  auto y1=ry-rh; if(y1<0) y1=0;
  auto x2=rx+rw; if(x2>w) x2=w;
  auto y2=ry+rh; if(y2>h) y2=h;
  l=(x2-x1) * (y2-y1);
  l=1.0 - l/(w * h);
  std::cerr << "lambda: " << l << "\n";
  return kget(torch::tensor({y1,y2,x1,x2},torch::kLong));
 KCATCH("cutmix");
}

Tensor blend(const Tensor& x,const Tensor& y,double r) {
 return (r*x + (1.0-r)*y).clamp(0.0,x.is_floating_point() ? 1.0 : 255.0).to(x.dtype());
}

Tensor brightness(const Tensor& x,double f) {
 TORCH_CHECK(f>=0, "brightness: factor must be non-negative, given ",f);
 return (f*x).clamp(0.0,x.is_floating_point() ? 1.0 : 255.0).to(x.dtype());
}

/*
def rand_brightness(x):
    x = x + (torch.rand(x.size(0), 1, 1, 1, dtype=x.dtype, device=x.device) - 0.5)
    return x
*/

Tensor grayscale(const Tensor& x) {
 return(0.2989 * x.select(-3,0) + 0.587 * x.select(-3,1) + 0.114 * x.select(-3,2)).to(x.dtype()).unsqueeze(-3);
 // r.expand(x.size())
 // contrast,saturation
}

Tensor contrast(const Tensor& x,double f) {
 TORCH_CHECK(f>=0, "contrast: factor must be non-negative, given ",f);
 auto c=x.size(-3);
 auto d=x.is_floating_point() ? x.scalar_type() : torch::kFloat;
 return blend(x, (c==3 ? grayscale(x) : x).mean({-3,-2,-1}, true, d), f);
}

Tensor saturation(const Tensor& x,double f) {
 TORCH_CHECK(f>=0, "saturation: factor must be non-negative, given ",f);
 // if 1 channel, return x
 return blend(x, grayscale(x), f);
}

/*
def rand_saturation(x):
    x_mean = x.mean(dim=1, keepdim=True)
    x = (x - x_mean) * (torch.rand(x.size(0), 1, 1, 1, dtype=x.dtype, device=x.device) * 2) + x_mean
    return x
*/

Tensor imageinvert(const Tensor& x) {
 return torch::tensor(x.is_floating_point() ? 1 : 255, TensorOptions().dtype(x.dtype()).device(x.device())) - x;
}

Tensor solarize(const Tensor& x,double f) {
 auto b=x.is_floating_point() ? 1.0 : 255.0;
 TORCH_CHECK(f<=b, "solarize: threshold should be less than ",b,", given ",f);
 return torch::where(x >= f, imageinvert(x), x);
}

Tensor blur(const Tensor& x) {
 auto d=x.is_floating_point() ? x.scalar_type() : torch::kFloat;
 auto k=torch::tensor({1,1,1,1,5,1,1,1,1}, TensorOptions().dtype(d).device(x.device()));
 k/=k.sum();
 k=k.reshape({3,3}).expand({x.size(-3),1,3,3});
 auto c=torch::nn::functional::conv2d(x.to(d),k,torch::nn::functional::Conv2dFuncOptions().groups(x.size(-3)));
 // convert c to x type
 auto r=x.clone();
 return c;
}

//https://github.com/lucidrains/lightweight-gan/blob/main/lightweight_gan/diff_augment.py

/*

    result_tmp, need_cast, need_squeeze, out_dtype = _cast_squeeze_in( img, [kernel.dtype,],)
    result_tmp = conv2d(result_tmp, kernel, groups=result_tmp.shape[-3])
    result_tmp = _cast_squeeze_out(result_tmp, need_cast, need_squeeze, out_dtype)

    result = img.clone()
    result[..., 1:-1, 1:-1] = result_tmp

    return result


def adjust_sharpness(img: Tensor, sharpness_factor: float) -> Tensor:
    if sharpness_factor < 0:
        raise ValueError(f"sharpness_factor ({sharpness_factor}) is not non-negative.")

    _assert_image_tensor(img)

    _assert_channels(img, [1, 3])

    if img.size(-1) <= 2 or img.size(-2) <= 2:
        return img

    return _blend(img, _blurred_degenerate_image(img), sharpness_factor)

*/

KAPI rtest(K x) {
 KTRY
  Tensor t;
  TORCH_CHECK(xten(x,t), "need tensor");
  auto u=torch::randint(3,10,torch::kLong);
  int64_t *r=u.data_ptr<int64_t>();
  std::cerr << u << "\n";
  for(const auto i:c10::irange(1000000)) {
   auto y=t+r[1]+i;
  }
  return (K)0;
 KCATCH("rtest");
}

KAPI isfloat(K x) {
 KTRY
  Tensor a;
  TORCH_CHECK(xten(x,a),"not a tensor");
  return kb(a.is_floating_point() || a.is_complex());
 KCATCH("isfloat");
}

KAPI cflags(K x) {
 std::cerr << torch::get_cxx_flags() << "\n";
 return (K)0;
}

KAPI applytest(K x,K y,K z) {
 KTRY
  torch::apply([](K x){ std::cerr << kstring(x) << "\n";}, x,y,z);
  return (K)0;
 KCATCH("applytest");
}

auto farray() {
 return std::array<int,3>{{1,2,3}};
}

KAPI maxarg(K x) {
 KTRY
  auto *m=xmodule(x);
  TORCH_CHECK(m, "need seqlist module");
  auto *q=m->module().as<knn::SeqList>();
  J N=0;
  TORCH_CHECK(q, "need seqlist module");
  for(size_t j=0; j<1000000; ++j) {
   for(size_t i=0; i<q->size(); ++i) {
    const auto& a=q->ptr<nn::SequentialImpl>(i);
    N+=knn::maxargs(*a->begin(),"seqlist");
   }
  }
  return kj(N);
 KCATCH("maxarg");
}

KAPI vtest(K x) {
 KTRY
  auto *v=xvec(x);
  TORCH_CHECK(v && v->size(),"not vector or empty vector!");
  TensorVector a=*v;
  a[0]=torch::tensor(-1);
  return (K)0;
 KCATCH("vtest");
}

KAPI amask(K x) {
 KTRY
  TORCH_CHECK(x->t == -KJ, "mask: need long scalar for mask size, given ",kname(x));
  TORCH_CHECK(x->j >= 0, "mask: size must be non-negative");
  // check IEEE754 support here since -inf is not guaranteed to be valid on non IEEE754 platform
  if (std::numeric_limits<float>::is_iec559) {
    return kten(torch::triu(torch::full({x->j, x->j}, -std::numeric_limits<float>::infinity()), 1));
  } else { // if IEEE754 is not supported, we use the smallest float number in current platform
    TORCH_WARN_ONCE("IEEE754 is not supported, mask will use smallest float number on this platform instead of -inf");
    return kten(torch::triu(torch::full({x->j, x->j}, std::numeric_limits<float>::lowest()), 1));
  }
 KCATCH("mask");
}

KAPI sizes_strides(K x,K y) {
 KTRY
  Tensor *t=xten(x);
  TORCH_CHECK(t,"supply tensor");
  IntArrayRef sz;
  TORCH_CHECK(xsize(y,sz), "supply size");
  t->unsafeGetTensorImpl()->set_sizes_and_strides(sz,{});
  return (K)0;
 KCATCH("sizes & strides");
}

/*
KAPI ganstep(K a) {
 KTRY
  Kmodel *d=xmodel(a,0), *g=xmodel(a,1);
  TORCH_CHECK(d && g, "ganstep: supply discriminator & generator model as 1st & 2nd args");
  TORCH_CHECK(d->o.c != Cast::lbfgs, "Cannout use lbfgs optimizer with discriminator");
  TORCH_CHECK(g->o.c != Cast::lbfgs, "Cannout use lbfgs optimizer with generator");
  Tensor* x=xten(a,1); Tensor* y=xten(a,2); Tensor* z=xten(a,3);
  d->opt().zero_grad();
  Tensor l0=mloss(d, *x, (*y)[0]);
  l0.backward();
  Tensor gx=c10::get<Tensor>(mforward(g->kmodule(),*z));
  Tensor l1=mloss(d, gx.detach(), (*y)[1]);
  l1.backward();
  d->opt().step();
  g->opt().zero_grad();
  Tensor l2=mloss(d, gx, (*y)[2]);
  l2.backward();
  g->opt().step();
  return(K)0;
 KCATCH("ganstep");
}
*/

KAPI kaiming(K x) {
 KTRY
  Tensor *t=xten(x);
  TORCH_CHECK(t,"not a tensor");
  torch::manual_seed(123);
  torch::nn::init::kaiming_normal_(*t, 0, torch::kFanOut, torch::kReLU);
  return (K)0;
 KCATCH("kaiming");
}

J sbytes(const Tensor& t,std::unordered_set<intptr_t>& u) {
 if(t.use_count()>1 || t.storage().use_count()>1) { // multiple references
  auto p=(intptr_t)t.storage().data();              // get integer pointer
  if(u.count(p)) {                                  // if seen before
   return 0;                                        // don't count bytes
  } else {                                          // else
   u.emplace(p);                                    // add pointer to set
   return t.storage().nbytes();                     // return bytes allocated
  }
 } else {
  return t.storage().nbytes();                      // no multiple references
 }
}

template<typename V>J sbytes(const V& v) {
  J n=0; std::unordered_set<intptr_t> u;
  for(const auto& t:v)
   if(t.defined())
    n += sbytes(t,u);
  return n;
}

J dbytes(const TensorDict& d) {
  J n=0; std::unordered_set<intptr_t> u;
  for(const auto& a:d)
   if(a.value().defined())
    n += sbytes(a.value(),u);
  return n;
}

//J vecbytes(const TensorVector& v) {
template<typename V>J vecbytes(const V& v) {
  J n=0; std::unordered_set<intptr_t> s;
  for(size_t i=0; i<v.size(); ++i) {
   if(v[i].storage().use_count()>1) {       // more than 1 tensor uses the storage
    auto p=(intptr_t)v[i].storage().data(); // get integer pointer
    if(!s.count(p)) {                       // if not seen before
     n += v[i].storage().nbytes();          // add the bytes allocated
     s.emplace(p);                          // add pointer to set
    }
   } else {
    n += v[i].storage().nbytes();
   }
  }
  return n;
}

KAPI bytes2(K x) {
 KTRY
  Kmodule *m=xmodule(x);
  auto *v=xvec(x);
  auto *d=xtensordict(x);
  if(m)
   return kj(sbytes(m->m->parameters()));
  else if(v)
   return kj(sbytes(*v));
  else if(d)
   return kj(dbytes(*d));
  else
   TORCH_ERROR("not module/vector");
 KCATCH("bytes2");
}

KAPI contigflag(K x) {
 KTRY
  Attr a=Attr::contiguous; K y=nullptr;
  Ktag *g=xtag(x);
  if(!g) {
   g=xtag(x,0);
   TORCH_CHECK(!g || x->n==2, mapattr(a),": expecting up to 2 args, given ",x->n);
   y=kK(x)[1];
  }
  TORCH_CHECK(g, mapattr(a),": expecting object, e.g. tensor, vector, module");
  TORCH_CHECK(g->a==Class::tensor,": not a tensor");
  TORCH_CHECK(!y || y->t==-KS, mapattr(a),": additional arg not a symbol");
  const Tensor& t=g->tensor();
  return kb(y ? t.is_contiguous(optmemory(y->s)) : t.is_contiguous());
 KCATCH("contiguous");
}

KAPI tf32(K x) {
 KTRY
 TORCH_CHECK(x->t==-KB,"need boolean scalar");
  std::cerr << " cuBLAS: " << torch::globalContext().allowTF32CuBLAS() << "\n";
  std::cerr << " cuDNN:  " << torch::globalContext().allowTF32CuDNN() << "\n";
  torch::globalContext().setAllowTF32CuBLAS(x->g);
  torch::globalContext().setAllowTF32CuDNN(x->g);
  std::cerr << " cuBLAS: " << torch::globalContext().allowTF32CuBLAS() << "\n";
  std::cerr << " cuDNN:  " << torch::globalContext().allowTF32CuDNN() << "\n";
  return (K)0;
 KCATCH("tf32");
}

KAPI coo(K x) {
 auto o=torch::dtype(torch::kDouble).device(torch::kCUDA);
 auto t1=torch::sparse_coo_tensor({2,3}, o);
 std::cerr << t1 << "\n";

 auto i=torch::tensor({{1},{1}});
 auto v=torch::tensor({1});
 auto t2=torch::sparse_coo_tensor(i,v,o);
 std::cerr << t2 << "\n";

 return (K)0;
}

KAPI sdim(K x,K y) {
 KTRY
  Tensor *t=xten(x);
  TORCH_CHECK(t, "tensor for 1st arg");
  TORCH_CHECK(y->t==-KJ, "long dim for 2nd arg");
  //return kten(torch::native::dense_to_sparse(*t,y->j));
  return kten(t->to_sparse(y->j));
 KCATCH("test dense_to_sparse");
}

KAPI optparse(K x) {
 KTRY
  TensorOptions o;
  TORCH_CHECK(xopt(x,o), "need tensor options");
  return optmap(o);
 KCATCH("optparse");
}

bool pincheck(const Tensor& t) {
 return t.is_sparse() ? false : t.is_pinned();
}

KAPI otest(K x) {
 KTRY
  auto *t=xten(x);
  TORCH_CHECK(t,"tensor required");
  const auto& o=t->options();
  std::cerr << "device: " << t->device() <<        "\t option: " << o.device()  << "\n";
  std::cerr << " dtype: " << t->dtype()  <<        "\t option: " << o.dtype()   << "\n";
  std::cerr << "layout: " << t->layout() <<        "\t option: " << o.layout()   << "\n";
  std::cerr << "  grad: " << t->requires_grad() << "\t option: " << o.requires_grad() << "\n";
  std::cerr << "pinned: " << pincheck(*t)        << "\t option: " << o.pinned_memory() << "\n";
  std::cerr << "memory: " << t->suggest_memory_format() << "\t option: " << (o.memory_format_opt().has_value() ? o.memory_format_opt().value() : torch::MemoryFormat::Contiguous) << "\n";
  return (K)0;
 KCATCH("options test");
}

static void gradmode() {
 if(torch::GradMode::is_enabled())  //torch::autograd::GradMode::is_enabled())
  std::cerr << "Grad Mode Enabled\n";
 else
  std::cerr << "Grad Mode Disabled\n";
}

KAPI gmode(K x) {
 gradmode();
 if(true) {
  torch::NoGradGuard g;
  gradmode();
 }
 gradmode();
 return (K)0;
}

/*
d=torch.nn.ParameterDict()
for k,v in model.named_parameters():
  d[k.replace('.','_')]=v
 
torch.jit.save(torch.jit.script(d),"/tmp/gpt")
*/

KAPI jfile(K x) {
 KTRY
  TORCH_CHECK(x->t==-KS, "need symbol");
  torch::jit::script::Module j = torch::jit::load(x->s);
  TensorDict d;
  for(const auto& a:j.named_parameters())
   d.insert(a.name,a.value);
  return kdict(d);
 KCATCH("load file");
}

std::vector<char> getbytes(std::string f) {
    std::ifstream s(f, std::ios::binary);
    std::vector<char> v((std::istreambuf_iterator<char>(s)), (std::istreambuf_iterator<char>()));
    s.close();
    return v;
}

KAPI loadfile(K x) {
 KTRY
  TORCH_CHECK(x->t==-KS, "need symbol");
  auto v=getbytes(x->s);
  std::cerr << "read file: " << x->s << ", " << v.size() << " byte(s)\n";
  torch::IValue a = torch::pickle_load(v);
  //  torch::Tensor my_tensor = x.toTensor();
  return (K)0;
 KCATCH("load file");
}

static void dups() {
 torch::nn::Linear m(1,1);
 torch::optim::SGD o(m->parameters(),.1);
 torch::optim::OptimizerParamGroup g(m->parameters());
 o.add_param_group(g);
 auto& p=o.param_groups();
 if(p[0].params()[1].is_same(p[1].params()[1]))
  std::cerr << "same parameter across different groups\n";
}

KAPI duptest(K x) {
 KTRY
 dups();
 return (K)0;
 KCATCH("dups");
}
 
KAPI optdefaults(K x) {
using Adagrad        = torch::optim::Adagrad;
using AdagradOptions = torch::optim::AdagradOptions;
 auto o=AdagradOptions(1.0);
 std::vector<torch::Tensor> v;
 for (size_t i = 0; i < 3; i++)
  v.push_back(torch::randn(10));
 auto a = Adagrad(v);
 a.param_groups()[0].set_options(std::make_unique<AdagradOptions>(o));
 a.defaults()=o;
 // test for defaults() method with non-const reference
 auto& d=static_cast<AdagradOptions&>(a.defaults());
 std::cerr << "defaults match specified options: " << (d == o) << "\n";
 std::cerr << "defaults match default options: " << (d == AdagradOptions()) << "\n";
 return (K)0;
}

J nest(K x) {
 if(x->t || !x->n) return 0;
 J n,m=0;
 for(J i=0;i<x->n;++i)
  if((n=nest(kK(x)[i])) && n>m) m=n;
 return ++m;
}

KAPI xnest(K x) {return kj(nest(x)); }

/*
//#include  <c10/cuda/CUDAUtils.h>
KAPI dtest(K x) {
 auto d=c10::cuda::current_device();
 std::cerr << d << "\n";
 return(K)0;
}
*/

void xerror(const char* s,K x) {
 if(x->t) {
  TORCH_ERROR(s, kname(x));
 } else {
  switch(x->n) {
   case 0:  TORCH_ERROR(s, "empty list");
   case 1:  TORCH_ERROR(s, "1-element list containing ", kname(x));
   case 2:  TORCH_ERROR(s, "2-element list containing ", kname(kK(x)[0]), " and ", kname(kK(x)[1]));
   default: TORCH_ERROR(s, x->n, "-element list containing ", kname(kK(x)[0]), ", ", kname(kK(x)[1]),", ..");
  }
 }
}

KAPI knulltest(K x) {
 K r=ktn(0,0);
 std::cerr << "count of r: " << r->n << "\n";
 jk(&r,r1(x));
 std::cerr << "count of r: " << r->n << "\n";
 return r;
}

K findbuffer(K x,const std::string &s,short t=nh);
K findbuffer(K x,const std::string &s,short t) {
 TORCH_CHECK(xdict(x), "dictionary expected, ",kname(x)," given, unable to find parameter ",s);
 K k=kK(x)[0], v=kK(x)[1]; J i=kfind(k,s);
 if(i<0)
  return nullptr;
 TORCH_CHECK(!v->t, "general list of values expected, ",kname(v)," given, unable to find parameter ",s);
 K r=kK(v)[i];
 TORCH_CHECK(t==nh || t==r->t, s,": ",kname(t)," expected, ",kname(r->t)," supplied");
 return xnull(r) ? nullptr : r;
}

/*
template <class... Fs> struct overload;

template <class F0, class... Frest> struct overload<F0, Frest...> : F0, overload<Frest...> {
    overload(F0 f0, Frest... rest) : F0(f0), overload<Frest...>(rest...) {}
    using F0::operator();
    using overload<Frest...>::operator();
};

template <class F0> struct overload<F0> : F0 {
    overload(F0 f0) : F0(f0) {}
    using F0::operator();
};

template <class... Fs> auto make_overload(Fs... fs) {
    return overload<Fs...>(fs...);
}
*/

//template <class... F> struct overload : F... {overload(F... f) : F(f)... {}};
//template <class... F> auto make_overload(F... f) {return overload<F...>(f...);}

/*
void testover(auto& x,auto& y) {
 auto f=make_overload(
   [](J& x, J& y) {std::cerr << x+y <<"\n"},
   [](auto& x, auto& y) {std::cerr << "auto\n"}),
   x,y);
}
*/

KAPI ksizes(K x) {
 std::cerr << "type_info: " << sizeof(std::type_info) << "\n";
 J j; auto h=typeid(j).hash_code();
 std::vector<TensorVector> v(3);
 std::cerr << "hash_code: " << sizeof(h) << "\n";
 std::cerr << "k0:      " << sizeof(k0) << "\n";
 std::cerr << "Tensor:  " << sizeof(Tensor) << "\n";
 std::cerr << "Module:  " << sizeof(Module) << "\n";
 std::cerr << "Class:   " << sizeof(Class) << "\n";
 std::cerr << "Cast:    " << sizeof(Cast) << "\n";
 std::cerr << "Ktag:    " << sizeof(Ktag) << "\n";
 std::cerr << "Kten:    " << sizeof(Kten) << "\n";
 std::cerr << "Kvec:    " << sizeof(Kvec) << "\n";
 std::cerr << "Kdict:   " << sizeof(Kdict) << "\n";
 std::cerr << "Kmodule: " << sizeof(Kmodule) << "\n";
 std::cerr << "Kopt:    " << sizeof(Kopt) << "\n";
 std::cerr << "Kmodel:  " << sizeof(Kmodel) << "\n";
 std::cerr << "Tuple:   " << sizeof(Tuple) << "\n";
 std::cerr << "Moduleptr: " << sizeof(Moduleptr) << "\n";
 std::cerr << "TensorVector: " << sizeof(TensorVector) << "\n";
 std::cerr << "TensorDict: " << sizeof(TensorDict) << "\n";
 std::cerr << "Input:  " << sizeof(Input) << "\n";
 std::cerr << "Output:  " << sizeof(Output) << "\n";
 std::cerr << "optional bool:  " << sizeof(c10::optional<bool>) << "\n";
 std::cerr << "optional int64: " << sizeof(c10::optional<int64_t>) << "\n";
 std::cerr << "Training options: " << sizeof(TrainOptions) << "\n";
 std::cerr << "Small TensorVector(5): " << sizeof(torch::SmallVector<Tensor,5>) << "\n";
 std::cerr << "Small TensorVector(7): " << sizeof(torch::SmallVector<Tensor,7>) << "\n";
 std::cerr << "Small TensorVector(9): " << sizeof(torch::SmallVector<Tensor,9>) << "\n";
 std::cerr << "vector of vectors: " << sizeof(v) << "\n";
 std::cerr << "size of Data: " << sizeof(Data) << "\n";
 std::cerr << "size of optional Data: " << sizeof(c10::optional<Data>) << "\n";
 return (K)0;
}

KAPI randint_type(K x) {
 std::cerr << torch::randint(10,{3}) << "\n";
 return (K)0;
}

//#include <c10/cuda/CUDAMacros.h>
//#include <c10/cuda/CUDACachingAllocator.h>

// check for cuda via USE_CUDA
// #ifdef USE_CUDA
//  ..
// #endif
/*
namespace c10 {
namespace cuda {
namespace CUDACachingAllocator {
C10_CUDA_API void emptyCache();
C10_CUDA_API uint64_t currentMemoryAllocated(int device);
C10_CUDA_API uint64_t maxMemoryAllocated(int device);
C10_CUDA_API void     resetMaxMemoryAllocated(int device);
C10_CUDA_API uint64_t currentMemoryCached(int device);
C10_CUDA_API uint64_t maxMemoryCached(int device);
C10_CUDA_API void     resetMaxMemoryCached(int device);
}}}
*/

/*
cache      
memory     e.g. memory() or memory`cuda or memory 0
maxcache   
maxmemory  
emptycache
resetcache 
resetmemory
*/
KAPI cudamem(K x) {
 KTRY
  // if sym, get device no
  // if int, verify -1<n< env.cuda
  //auto n=c10::cuda::CUDACachingAllocator::currentMemoryAllocated(x->j);
  //return kj(n);
  return kj(nj);
 KCATCH("cuda memory");
}

#define ENUMTEST(name) \
{ \
  v = torch::k##name; \
  std::cerr << torch::enumtype::get_enum_name(v) << " " << ESYM(v) << "\n"; \
}

KAPI enumtest(K x) {
  c10::variant<
    torch::enumtype::kLinear,
    torch::enumtype::kConv1D,
    torch::enumtype::kConv2D,
    torch::enumtype::kConv3D,
    torch::enumtype::kConvTranspose1D,
    torch::enumtype::kConvTranspose2D,
    torch::enumtype::kConvTranspose3D,
    torch::enumtype::kSigmoid,
    torch::enumtype::kTanh,
    torch::enumtype::kReLU,
    torch::enumtype::kLeakyReLU,
    torch::enumtype::kFanIn,
    torch::enumtype::kFanOut,
    torch::enumtype::kGELU,
    torch::enumtype::kConstant,
    torch::enumtype::kReflect,
    torch::enumtype::kReplicate,
    torch::enumtype::kCircular,
    torch::enumtype::kNearest,
    torch::enumtype::kNearestExact,
    torch::enumtype::kBilinear,
    torch::enumtype::kBicubic,
    torch::enumtype::kTrilinear,
    torch::enumtype::kArea,
    torch::enumtype::kSum,
    torch::enumtype::kMean,
    torch::enumtype::kMax,
    torch::enumtype::kNone,
    torch::enumtype::kBatchMean,
    torch::enumtype::kZeros,
    torch::enumtype::kBorder,
    torch::enumtype::kReflection,
    torch::enumtype::kMish,
    torch::enumtype::kSame,
    torch::enumtype::kSiLU,
    torch::enumtype::kValid
  > v;

  ENUMTEST(Linear)
  ENUMTEST(Conv1D)
  ENUMTEST(Conv2D)
  ENUMTEST(Conv3D)
  ENUMTEST(ConvTranspose1D)
  ENUMTEST(ConvTranspose2D)
  ENUMTEST(ConvTranspose3D)
  ENUMTEST(Sigmoid)
  ENUMTEST(Tanh)
  ENUMTEST(ReLU)
  ENUMTEST(LeakyReLU)
  ENUMTEST(FanIn)
  ENUMTEST(FanOut)
  ENUMTEST(GELU)
  ENUMTEST(Constant)
  ENUMTEST(Reflect)
  ENUMTEST(Replicate)
  ENUMTEST(Circular)
  ENUMTEST(Nearest)
  ENUMTEST(NearestExact)
  ENUMTEST(Bilinear)
  ENUMTEST(Bicubic)
  ENUMTEST(Trilinear)
  ENUMTEST(Area)
  ENUMTEST(Sum)
  ENUMTEST(Mean)
  ENUMTEST(Max)
  ENUMTEST(None)
  ENUMTEST(BatchMean)
  ENUMTEST(Zeros)
  ENUMTEST(Border)
  ENUMTEST(Reflection)
  ENUMTEST(Mish)
  ENUMTEST(Same)
  ENUMTEST(SiLU)
  ENUMTEST(Valid)
 return (K)0;
}

KAPI kdata(K x,K y) {
 KTRY
  int64_t i=0;
  auto dataset = torch::data::datasets::MNIST(x->s)
    .map(torch::data::transforms::Normalize<>(0.5, 0.5))
    .map(torch::data::transforms::Stack<>());
  auto data_loader = torch::data::make_data_loader(std::move(dataset));
  for (torch::data::Example<>& batch : *data_loader) {
    if(i==y->j) {
     std::cout << batch.target << "\n";
     std::cout << batch.data   << "\n ";
     return kten(batch.data);
    }
  }
  return (K)0;
 KCATCH("mnist test");
}

void errfail() {
 if(true) {
  TORCH_ERROR("err");
 } else {
  TORCH_ERROR("false");
 }
}

KAPI testcount(K x,K y) {
 KTRY
 if(y->t != -KJ) return KERR("2nd arg must be offset");
 Pairs p; J i=y->j; J n=xargc(x,i,p);
 std::cerr << "arg count: " << n << ", pair count: " << p.n << "\n";
 return kb(xnone(x,i));
 KCATCH("test count");
}

KAPI testptr(K x) {
 Tensor t;
 if(xten(x,t))
  std::cerr<<"tensor\n";
 else if(xten(x,0,t) && x->n==1)
  std::cerr<<"enlisted tensor\n";
 else
  std::cerr<<"something else\n";
 return(K)0;
}

#define ASSERT_THROWS_WITH(statement, substring)                        \
  {                                                                     \
    std::string assert_throws_with_error_message;                       \
    try {                                                               \
      (void)statement;                                                  \
      std::cerr << "Expected statement `" #statement                       \
                "` to throw an exception, but it did not";              \
    } catch (const c10::Error& e) {                                     \
      assert_throws_with_error_message = e.what_without_backtrace();    \
    } catch (const std::exception& e) {                                 \
      assert_throws_with_error_message = e.what();                      \
    }                                                                   \
    if (assert_throws_with_error_message.find(substring) ==             \
        std::string::npos) {                                            \
      std::cerr << "Error message \"" << assert_throws_with_error_message  \
             << "\" did not contain expected substring \"" << substring \
             << "\"";                                                   \
    }                                                                   \
  }

KAPI namecheck(K x) {
 torch::nn::Sequential s; //size_t n=0;
 std::cout << "initial size: " << s->size() << "\n";
 ASSERT_THROWS_WITH(
      s->push_back("name.with.dot", torch::nn::Linear(3, 4)),
      "Submodule name must not contain a dot (got 'name.with.dot')");
  ASSERT_THROWS_WITH(
      s->push_back("", torch::nn::Linear(3, 4)),
      "Submodule name must not be empty");
  std::cout << "size after name errors: " << s->size() << "\n";
  std::cout << "size of modules: "        << s->modules(false).size() << "\n";
  std::cout << "size of named children: " << s->named_children().size() << "\n";

  return(K)0;
}

KAPI dupname(K x) {
KTRY
 nn::Sequential q(
  {{"A", torch::nn::Linear(1,2)},
   {"B", torch::nn::Conv2d(3,4,5)}});
 auto t=torch::randn({1,2});
 q->forward(t,t);
 return (K)0;
KCATCH("duplicate names");
}

KAPI kdictflag(K x) {
 return kb(xdict(x));
}

static K ksub(K x,const char* e) {
 KTRY
 std::cerr << "in ksub " << (!x ? "null" : "with args")<< "\n";
 Tensor t;
 if(!x || (x->t==-KS && x->s==cs("help"))) {
  std::cerr << " still in ksub " << (!x ? "null" : "with args")<< "\n";
  TORCH_ERROR(e," help here..");
 }

 if(xten(x,t)) {
  return kten(torch::max(t));
 } else {
  return ksub(nullptr,e);
 }
 KCATCH(e);
}

KAPI ktest(K x) {return ksub(x,"ktest()");}

KAPI pairtest(K x) {
 KTRY
 Pairs p;
 if(xpairs(x,p) || xpairs(x,x->n-1,p)) {
  switch(p.a) {
   case 1: std::cout << "dictionary["; break;
   case 2: std::cout << "pairs["; break;
   case 3: std::cout << "list["; break;
   case 4: std::cout << "symbol list["; break;
   default: std::cout << "unknown name,value structure["; break;
  }
  std::cout << p.n << "]\n";
  while(xpair(p)) {
   switch(p.t) {
    case -KB: std::cout << "boolean: " << p.k << " -> " << p.b << "\n"; break;
    case -KS: std::cout << " symbol: " << p.k << " -> " << p.s << "\n"; break;
    case -KJ: std::cout << "integer: " << p.k << " -> " << p.j << "\n"; break;
    case -KF: std::cout << " double: " << p.k << " -> " << p.f << "\n"; break;
    default:  std::cout << "  other: " << p.k << " -> " << kname(p.t) << "\n"; break;
   }
  }
  return kb(true);
 } else {
  return kb(false);
 }
 KCATCH("pairs test..");
}

KAPI lbfgs(K x) {
    int i, n=x->j;
    auto t=torch::randn({n});

    TensorVector v = {torch::randn({n}, torch::requires_grad())};
    //torch::optim::SGD o(v, /*lr=*/0.01);
    torch::optim::LBFGS o(v, 1);

    auto cost = [&](){
        o.zero_grad();
        auto d = torch::pow(v[0] - t, 2).sum();
        std::cerr << i << ") " << d.item().toDouble() << "\n";
        d.backward();
        return d;
    };

    for (i = 0; i < 5; ++i){
        o.step(cost);//for LBFGS
        //cost(); o.step(); // for SGD
    }
    return kget(torch::stack({t,v[0]}));
}

KAPI learn(K x) {
 KTRY
  Scalar s; Tensor t;
  if(xten(x,0,t) && xnum(x,1,s)) {
   if(t.grad().defined()) {
    torch::NoGradGuard g;
    //t.add_(-s.toDouble()*t.grad());
    t.add_(-s*t.grad());
    t.grad().zero_();
    return (K)0;
   } else {
    return KERR("no gradient defined");
   }
  } else {
   return KERR("unrecognized arg(s), expecting (tensor;learning rate)");
  }
 KCATCH("error applying learning rate and gradient to tensor");
}

KAPI opttest(K x) {
 Tensor t; TensorOptions o; xten(x,t);
 if(t.defined())
  o=o.device(t.device()).dtype(t.dtype());

 std::cout << "dtype:       " << o.dtype() << "\n";
 std::cout << "device:      " << o.device() << "\n";
 std::cout << "layout:      " << o.layout() << "\n";
 std::cout << "gradient:    " << o.requires_grad() << "\n";
 std::cout << "has dtype:   " << o.has_dtype()  << "\n";
 std::cout << "has device:  " << o.has_device() << "\n";
 std::cout << "has layout:  " << o.has_layout() << "\n";
 std::cout << "has grad:    " << o.has_requires_grad() << "\n";
 std::cout << "has pinned:  " << o.has_pinned_memory() << "\n";
 std::cout << "has memory:  " << o.has_memory_format() << "\n";
 return (K)0;
}

// tensor(`sparse;array)
// tensor(`sparse;array;mask)
// tensor(`sparse;size) -> tensor(`empty;3 4;`sparse)

/*
KAPI sparse(K x) {
 Tensor a,b; TensorOptions o;
 KTRY
  if(xten(x,a)) {
  } else if(xten(x,0,a) && xten(x,1,b) {
   if(x->n==2)
   else if(x->n==3)
   
    // no size
    // size
    // w'options
  } else if(x->n = {
  }
 
  return(K)0;
 KCATCH("sparse tensor error");
}
*/

KAPI to_sparse(K x) {
 if(auto* t=xten(x))
  return kten(t->to_sparse());
 else
  TORCH_ERROR("to_sparse not implemented for ",kname(x->t));
}


KAPI sparse1(K x) {
 auto m=kput(kK(x)[0]),t=kput(kK(x)[1]),v=torch::masked_select(t,m),i=torch::nonzero(m);
 //return kten(torch::sparse_coo_tensor(i.t(),v));
 return kten(torch::sparse_coo_tensor(i.t(),v,m.sizes()));
}

#ifdef __clang__
# pragma clang diagnostic pop
#endif
